---

## BitBucket repository link:

https://jarrous@bitbucket.org/jarrous/maid.cc.demo.git

--- 

## Clone Repository
to clone the repository, enter 'git clone' and the repository URL at your command line:

git clone https://jarrous@bitbucket.org/jarrous/maid.cc.demo.git 

---

## Run the application
to run the application, enter this line at your command line in the application directory:

mvn spring-boot:run

--- 