package Maid.cc.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.sql.DataSource;


@Configuration
@SpringBootApplication
@EnableJpaRepositories("Maid.cc.demo.repository")
@EntityScan("Maid.cc.demo.entity")
@ComponentScan({"Maid.cc.demo.config",
				"Maid.cc.demo.repository",
				"Maid.cc.demo.service",
				"Maid.cc.demo.controller"})
public class DemoApplication extends SpringBootServletInitializer implements CommandLineRunner {

	@Autowired
    DataSource dataSource;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Override
	public void run(String... args) {
		System.out.println("Server is running");
		System.out.println("Our DataSource is = " + dataSource);
	}
}
