package Maid.cc.demo.controller;

import Maid.cc.demo.dto.ClientDto;
import Maid.cc.demo.dto.ClientResponseDto;
import Maid.cc.demo.service.ClientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/client")
@Api(value = "Client Management")
public class ClientController {

    //region Properties

    @Autowired
    ClientService clientService;

    //endregion

    //region Public Methods

    //region Get All Client
    @ApiOperation(value = "fetch all clients from the database", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully fetched clients list"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "Internal Server Error, please contact support")
    })
    @GetMapping
    public ResponseEntity<List<ClientResponseDto>> getAll() {
        return clientService.getAllClients();
    }
    //endregion

    //region Add Client
    @ApiOperation(value = "add client", response = ClientResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "client added Successfully"),
            @ApiResponse(code = 500, message = "Internal Server Error, please contact support")
    })
    @PostMapping
    public ResponseEntity<ClientResponseDto> addClient(@RequestBody ClientDto dto) {
        return clientService.addClient(dto);
    }
    //endregion

    //region Update Client
    @ApiOperation(value = "update client", response = ClientResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "client updated Successfully"),
            @ApiResponse(code = 500, message = "Internal Server Error, please contact support")
    })
    @PutMapping(value ="{clientId}")
    public ResponseEntity<ClientResponseDto> updateClient(@PathVariable UUID clientId, @RequestBody ClientDto dto) {
        return clientService.updateClient(clientId, dto);
    }
    //endregion

    //endregion
}
