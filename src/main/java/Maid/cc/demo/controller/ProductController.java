package Maid.cc.demo.controller;

import Maid.cc.demo.dto.ProductDto;
import Maid.cc.demo.dto.ProductResponseDto;
import Maid.cc.demo.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/product")
@Api(value = "Product Management")
public class ProductController {

    //region Properties

    @Autowired
    ProductService productService;

    //endregion

    //region Public Methods

    //region Get All Product
    @ApiOperation(value = "fetch all products from the database", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully fetched products list"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "Internal Server Error, please contact support")
    })
    @GetMapping
    public ResponseEntity<List<ProductResponseDto>> getAll() {
        return productService.getAllProducts();
    }
    //endregion

    //region Add Product
    @ApiOperation(value = "add product", response = ProductResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "product added Successfully"),
            @ApiResponse(code = 500, message = "Internal Server Error, please contact support")
    })
    @PostMapping
    public ResponseEntity<ProductResponseDto> addProduct(@RequestBody ProductDto dto) {
        return productService.addProduct(dto);
    }
    //endregion

    //region Update Product
    @ApiOperation(value = "update product", response = ProductResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "product updated Successfully"),
            @ApiResponse(code = 500, message = "Internal Server Error, please contact support")
    })
    @PutMapping(value ="{productId}")
    public ResponseEntity<ProductResponseDto> updateProduct(@PathVariable UUID productId, @RequestBody ProductDto dto) {
        return productService.updateProduct(productId, dto);
    }
    //endregion

    //endregion
}
