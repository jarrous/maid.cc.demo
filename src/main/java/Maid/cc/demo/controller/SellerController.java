package Maid.cc.demo.controller;

import Maid.cc.demo.dto.SellerDto;
import Maid.cc.demo.dto.SellerResponseDto;
import Maid.cc.demo.service.SellerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/seller")
@Api(value = "Seller Management")
public class SellerController {

    //region Properties

    @Autowired
    SellerService sellerService;

    //endregion

    //region Public Methods

    //region Get All Seller
    @ApiOperation(value = "fetch all sellers from the database", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully fetched sellers list"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "Internal Server Error, please contact support")
    })
    @GetMapping
    public ResponseEntity<List<SellerResponseDto>> getAll() {
        return sellerService.getAllSellers();
    }
    //endregion

    //region Add Seller
    @ApiOperation(value = "add seller", response = SellerResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "seller added Successfully"),
            @ApiResponse(code = 500, message = "Internal Server Error, please contact support")
    })
    @PostMapping
    public ResponseEntity<SellerResponseDto> addSeller(@RequestBody SellerDto dto) {
        return sellerService.addSeller(dto);
    }
    //endregion

    //region Update Seller
    @ApiOperation(value = "update seller", response = SellerResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "seller updated Successfully"),
            @ApiResponse(code = 500, message = "Internal Server Error, please contact support")
    })
    @PutMapping(value ="{sellerId}")
    public ResponseEntity<SellerResponseDto> updateSeller(@PathVariable UUID sellerId, @RequestBody SellerDto dto) {
        return sellerService.updateSeller(sellerId, dto);
    }
    //endregion

    //endregion
}
