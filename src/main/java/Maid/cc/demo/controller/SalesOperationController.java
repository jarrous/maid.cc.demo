package Maid.cc.demo.controller;

import Maid.cc.demo.dto.SalesOperationDto;
import Maid.cc.demo.dto.SalesOperationResponseDto;
import Maid.cc.demo.dto.TransactionDto;
import Maid.cc.demo.service.SalesOperationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/salesOperation")
@Api(value = "SalesOperation Management")
public class SalesOperationController {

    //region Properties

    @Autowired
    SalesOperationService salesOperationService;

    //endregion

    //region Public Methods

    //region Get All SalesOperation
    @ApiOperation(value = "fetch all sales operations from the database", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully fetched sales operations list"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "Internal Server Error, please contact support")
    })
    @GetMapping
    public ResponseEntity<List<SalesOperationResponseDto>> getAll() {
        return salesOperationService.getAllSalesOperations();
    }
    //endregion

    //region Add SalesOperation
    @ApiOperation(value = "add salesOperation", response = SalesOperationResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "salesOperation added Successfully"),
            @ApiResponse(code = 500, message = "Internal Server Error, please contact support")
    })
    @PostMapping
    public ResponseEntity<SalesOperationResponseDto> addSalesOperation(@RequestBody SalesOperationDto dto) {
        return salesOperationService.addSalesOperation(dto);
    }
    //endregion

    //region Update Transaction of SalesOperation
    @ApiOperation(value = "update transaction of sales operation", response = SalesOperationResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "transaction updated Successfully"),
            @ApiResponse(code = 500, message = "Internal Server Error, please contact support")
    })
    @PutMapping(value ="{salesOperationId}/transaction/{transactionId}")
    public ResponseEntity<SalesOperationResponseDto> updateSalesOperation(@PathVariable UUID salesOperationId, @PathVariable UUID transactionId, @RequestBody TransactionDto dto) {
        return salesOperationService.updateTransaction(salesOperationId, transactionId, dto);
    }
    //endregion

    //endregion
}
