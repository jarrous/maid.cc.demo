package Maid.cc.demo.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class SalesOperation extends BaseEntity{

    //region Properties

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="client_id")
    private Client client;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="seller_id")
    private Seller seller;

    @OneToMany(mappedBy = "salesOperation", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Transaction> transactions;

    //endregion

    //region Constructors

    public SalesOperation() {
        this.transactions = new HashSet<>();
    }

    public SalesOperation(Client client, Seller seller) {
        this.client = client;
        this.seller = seller;
        this.transactions = new HashSet<>();
    }

    public SalesOperation(Client client, Seller seller, Set<Transaction> transactions) {
        this.client = client;
        this.seller = seller;
        this.transactions = transactions;
    }

    //endregion

    //region Getters & Setters

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public Set<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }

    //endregion

    //region Public Methods

    public void addTransaction(Transaction transaction){
        this.transactions.add(transaction);
        transaction.setSalesOperation(this);
    }

    public void removeTransaction(Transaction transaction){
        this.transactions.remove(transaction);
        transaction.setSalesOperation(null);
    }

    //endregion
}
