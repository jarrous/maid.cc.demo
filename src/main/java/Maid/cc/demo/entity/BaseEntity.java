package Maid.cc.demo.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.UUID;

@MappedSuperclass
public class BaseEntity {

    //region Properties

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column( name = "id", columnDefinition = "BINARY(16)")
    protected UUID id;

    @CreationTimestamp
    protected Timestamp createdAt;

    @UpdateTimestamp
    protected Timestamp updatedAt;

    //endregion

    //region Constructor

    public BaseEntity() {
    }

    //endregion

    //region Getters

    public UUID getId() {
        return id;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    //endregion
}
