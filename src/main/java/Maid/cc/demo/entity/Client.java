package Maid.cc.demo.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Client extends BaseEntity{

    //region Properties

    private String name;

    private String lastName;

    private String mobileNumber;

    @OneToMany(mappedBy = "client", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<SalesOperation> salesOperations;

    //endregion

    //region Constructors

    public Client() {
        this.salesOperations = new HashSet<>();
    }

    public Client(String name, String lastName, String mobileNumber) {
        this.name = name;
        this.lastName = lastName;
        this.mobileNumber = mobileNumber;
        this.salesOperations = new HashSet<>();
    }

    public Client(String name, String lastName, String mobileNumber, Set<SalesOperation> salesOperations) {
        this.name = name;
        this.lastName = lastName;
        this.mobileNumber = mobileNumber;
        this.salesOperations = salesOperations;
    }

    //endregion

    //region Getters & Setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Set<SalesOperation> getSalesOperations() {
        return salesOperations;
    }

    public void setSalesOperations(Set<SalesOperation> salesOperations) {
        this.salesOperations = salesOperations;
    }

    //endregion

    //region Public Methods

    public void addSalesOperation(SalesOperation salesOperation){
        this.salesOperations.add(salesOperation);
        salesOperation.setClient(this);
    }

    public void removeSalesOperation(SalesOperation salesOperation){
        this.salesOperations.remove(salesOperation);
        salesOperation.setClient(null);
    }

    //endregion
}
