package Maid.cc.demo.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Product extends BaseEntity{

    //region Properties

    private String name;

    private String description;

    private String category;

    @OneToMany(mappedBy = "product", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Transaction> transactions;

    //endregion

    //region Constructors

    public Product() {
        this.transactions = new HashSet<>();
    }

    public Product(String name, String description, String category) {
        this.name = name;
        this.description = description;
        this.category = category;
        this.transactions = new HashSet<>();
    }

    public Product(String name, String description, String category, Set<Transaction> transactions) {
        this.name = name;
        this.description = description;
        this.category = category;
        this.transactions = transactions;
    }

    //endregion

    //region Getters & Setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Set<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }

    //endregion

    //region Public Methods

    public void addTransaction(Transaction transaction){
        this.transactions.add(transaction);
        transaction.setProduct(this);
    }

    public void removeTransaction(Transaction transaction){
        this.transactions.remove(transaction);
        transaction.setProduct(null);
    }

    //endregion
}
