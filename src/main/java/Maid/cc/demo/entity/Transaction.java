package Maid.cc.demo.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Transaction extends BaseEntity {

    //region Properties

    private long quantity;

    private double price;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="product_id")
    private Product product;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="sales_operation_id")
    private SalesOperation salesOperation;

    //endregion

    //region Constructors

    public Transaction() {
    }

    public Transaction(long quantity, double price, Product product, SalesOperation salesOperation) {
        this.quantity = quantity;
        this.price = price;
        this.product = product;
        this.salesOperation = salesOperation;
    }

    //endregion

    //region Getters & Setters

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public SalesOperation getSalesOperation() {
        return salesOperation;
    }

    public void setSalesOperation(SalesOperation salesOperation) {
        this.salesOperation = salesOperation;
    }

    //endregion
}
