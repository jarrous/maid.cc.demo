package Maid.cc.demo.dto;

import java.sql.Timestamp;
import java.util.UUID;

public class ProductResponseDto {

    //region Properties

    public UUID id;

    public Timestamp createdAt;

    public String name;

    public String description;

    public String category;

    //endregion

    //region Constructors

    public ProductResponseDto() {
    }

    public ProductResponseDto(UUID id, Timestamp createdAt, String name, String description, String category) {
        this.id = id;
        this.createdAt = createdAt;
        this.name = name;
        this.description = description;
        this.category = category;
    }

    //endregion
}
