package Maid.cc.demo.dto;

import java.util.UUID;

public class TransactionResponseDto {

    //region Properties

    public UUID id;

    public long quantity;

    public double price;

    public UUID productId;

    public String productName;

    public UUID salesOperationId;

    //endregion

    //region Constructors

    public TransactionResponseDto() {
    }

    public TransactionResponseDto(UUID id, long quantity, double price, UUID productId,
                                  String productName, UUID salesOperationId) {
        this.id = id;
        this.quantity = quantity;
        this.price = price;
        this.productId = productId;
        this.productName = productName;
        this.salesOperationId = salesOperationId;
    }

    //endregion
}
