package Maid.cc.demo.dto;

import java.sql.Timestamp;
import java.util.Set;
import java.util.UUID;

public class SalesOperationResponseDto {

    //region Properties

    public UUID id;

    public Timestamp createdAt;

    public UUID clientId;

    public String clientName;

    public UUID sellerId;

    public String sellerName;

    public double total;

    public Set<TransactionResponseDto> transactions;

    //endregion

    //region Constructors

    public SalesOperationResponseDto() {
    }

    public SalesOperationResponseDto(UUID id, Timestamp createdAt, UUID clientId, String clientName,
                                     UUID sellerId, String sellerName, double total,
                                     Set<TransactionResponseDto> transactions) {
        this.id = id;
        this.createdAt = createdAt;
        this.clientId = clientId;
        this.clientName = clientName;
        this.sellerId = sellerId;
        this.sellerName = sellerName;
        this.total = total;
        this.transactions = transactions;
    }

    //endregion
}
