package Maid.cc.demo.dto;

import java.util.UUID;

public class TransactionDto {

    //region Properties

    public long quantity;

    public double price;

    public UUID productId;

    //endregion
}
