package Maid.cc.demo.dto;

import java.sql.Timestamp;
import java.util.UUID;

public class ClientResponseDto {

    //region Properties

    public UUID id;

    public Timestamp createdAt;

    public String name;

    public String lastName;

    public String mobileNumber;

    //endregion

    //region Constructors

    public ClientResponseDto() {
    }

    public ClientResponseDto(UUID id, Timestamp createdAt, String name, String lastName, String mobileNumber) {
        this.id = id;
        this.createdAt = createdAt;
        this.name = name;
        this.lastName = lastName;
        this.mobileNumber = mobileNumber;
    }

    //endregion
}
