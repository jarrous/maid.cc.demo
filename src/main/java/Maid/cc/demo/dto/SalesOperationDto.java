package Maid.cc.demo.dto;

import java.util.List;
import java.util.UUID;

public class SalesOperationDto {

    //region Properties

    public UUID clientId;

    public UUID sellerId;

    public List<TransactionDto> transactions;

    //endregion
}
