package Maid.cc.demo.repository;

import Maid.cc.demo.entity.Seller;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface SellerRepository extends CrudRepository<Seller, UUID> {
}
