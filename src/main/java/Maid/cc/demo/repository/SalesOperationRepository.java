package Maid.cc.demo.repository;

import Maid.cc.demo.entity.SalesOperation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface SalesOperationRepository extends CrudRepository<SalesOperation, UUID> {
}
