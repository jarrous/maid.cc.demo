package Maid.cc.demo.service;

import Maid.cc.demo.dto.ProductResponseDto;
import Maid.cc.demo.dto.ProductDto;
import Maid.cc.demo.entity.Product;
import Maid.cc.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class ProductService {

    //region Properties

    @Autowired
    ProductRepository productRepository;

    //endregion

    //region Public Methods

    public ResponseEntity<List<ProductResponseDto>> getAllProducts(){
        try{

            //region get all products in database
            List<Product> products = (List<Product>)productRepository.findAll();
            //endregion

            //region return list of response dtos
            List<ProductResponseDto> productResponseDtos = toProductResponseDtos(products);
            return new ResponseEntity<>(productResponseDtos, HttpStatus.ACCEPTED);
            //endregion

        }catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<ProductResponseDto> addProduct(ProductDto dto){
        try{

            //region convert dto to entity
            Product product = toProductEntity(dto);
            //endregion

            //region save entity
            Product savedProduct = productRepository.save(product);
            //endregion

            //region return response dto
            ProductResponseDto productResponseDto = toProductResponseDto(savedProduct);
            return new ResponseEntity<>(productResponseDto, HttpStatus.ACCEPTED);
            //endregion

        }catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<ProductResponseDto> updateProduct(UUID productId, ProductDto updateDto){
        try{

            //region find product by id
            Optional<Product> findProductResult = productRepository.findById(productId);
            if (!findProductResult.isPresent()){
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }

            Product product = findProductResult.get();
            //endregion

            //region update product fields
            product.setName(updateDto.name);
            product.setDescription(updateDto.description);
            product.setCategory(updateDto.category);
            Product savedProduct = productRepository.save(product);
            //endregion

            //region return response dto
            ProductResponseDto productResponseDto = toProductResponseDto(savedProduct);
            return new ResponseEntity<>(productResponseDto, HttpStatus.ACCEPTED);
            //endregion

        }catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //endregion

    //region Private Methods

    private List<ProductResponseDto> toProductResponseDtos(List<Product> products){
        return products.stream()
                .map(product -> toProductResponseDto(product))
                .collect(Collectors.toList());
    }

    private ProductResponseDto toProductResponseDto(Product product){
        return new ProductResponseDto(
                product.getId(),
                product.getCreatedAt(),
                product.getName(),
                product.getDescription(),
                product.getCategory());
    }

    private Product toProductEntity(ProductDto dto){
        return new Product(dto.name, dto.description, dto.category);
    }

    //endregion
}
