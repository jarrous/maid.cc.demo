package Maid.cc.demo.service;

import Maid.cc.demo.dto.ClientDto;
import Maid.cc.demo.dto.ClientResponseDto;
import Maid.cc.demo.entity.Client;
import Maid.cc.demo.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class ClientService {

    //region Properties

    @Autowired
    ClientRepository clientRepository;

    //endregion

    //region Public Methods

    public ResponseEntity<List<ClientResponseDto>> getAllClients(){
        try{

            //region get all clients in database
            List<Client> clients = (List<Client>)clientRepository.findAll();
            //endregion

            //region return list of response dtos
            List<ClientResponseDto> clientResponseDtos = toClientResponseDtos(clients);
            return new ResponseEntity<>(clientResponseDtos, HttpStatus.ACCEPTED);
            //endregion

        }catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<ClientResponseDto> addClient(ClientDto dto){
        try{

            //region convert dto to entity
            Client client = toClientEntity(dto);
            //endregion

            //region save entity
            Client savedClient = clientRepository.save(client);
            //endregion

            //region return response dto
            ClientResponseDto clientResponseDto = toClientResponseDto(savedClient);
            return new ResponseEntity<>(clientResponseDto, HttpStatus.ACCEPTED);
            //endregion

        }catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<ClientResponseDto> updateClient(UUID clientId, ClientDto updateDto){
        try{

            //region find client by id
            Optional<Client> findClientResult = clientRepository.findById(clientId);
            if (!findClientResult.isPresent()){
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }

            Client client = findClientResult.get();
            //endregion

            //region update client fields
            client.setName(updateDto.name);
            client.setLastName(updateDto.lastName);
            client.setMobileNumber(updateDto.mobileNumber);
            Client savedClient = clientRepository.save(client);
            //endregion

            //region return response dto
            ClientResponseDto clientResponseDto = toClientResponseDto(savedClient);
            return new ResponseEntity<>(clientResponseDto, HttpStatus.ACCEPTED);
            //endregion

        }catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //endregion

    //region Private Methods

    private List<ClientResponseDto> toClientResponseDtos(List<Client> clients){
        return clients.stream()
                .map(client -> toClientResponseDto(client))
                .collect(Collectors.toList());
    }

    private ClientResponseDto toClientResponseDto(Client client){
        return new ClientResponseDto(
                client.getId(),
                client.getCreatedAt(),
                client.getName(),
                client.getLastName(),
                client.getMobileNumber());
    }

    private Client toClientEntity(ClientDto dto){
        return new Client(dto.name, dto.lastName, dto.mobileNumber);
    }

    //endregion
}
