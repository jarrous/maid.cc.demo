package Maid.cc.demo.service;

import Maid.cc.demo.dto.SellerDto;
import Maid.cc.demo.dto.SellerResponseDto;
import Maid.cc.demo.entity.Seller;
import Maid.cc.demo.repository.SellerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class SellerService {

    //region Properties

    @Autowired
    SellerRepository sellerRepository;

    //endregion

    //region Public Methods

    public ResponseEntity<List<SellerResponseDto>> getAllSellers(){
        try{

            //region get all sellers in database
            List<Seller> sellers = (List<Seller>)sellerRepository.findAll();
            //endregion

            //region return list of response dtos
            List<SellerResponseDto> sellerResponseDtos = toSellerResponseDtos(sellers);
            return new ResponseEntity<>(sellerResponseDtos, HttpStatus.ACCEPTED);
            //endregion

        }catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<SellerResponseDto> addSeller(SellerDto dto){
        try{

            //region convert dto to entity
            Seller seller = toSellerEntity(dto);
            //endregion

            //region save entity
            Seller savedSeller = sellerRepository.save(seller);
            //endregion

            //region return response dto
            SellerResponseDto sellerResponseDto = toSellerResponseDto(savedSeller);
            return new ResponseEntity<>(sellerResponseDto, HttpStatus.ACCEPTED);
            //endregion

        }catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<SellerResponseDto> updateSeller(UUID sellerId, SellerDto updateDto){
        try{

            //region find seller by id
            Optional<Seller> findSellerResult = sellerRepository.findById(sellerId);
            if (!findSellerResult.isPresent()){
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }

            Seller seller = findSellerResult.get();
            //endregion

            //region update seller fields
            seller.setName(updateDto.name);
            seller.setLastName(updateDto.lastName);
            seller.setMobileNumber(updateDto.mobileNumber);
            Seller savedSeller = sellerRepository.save(seller);
            //endregion

            //region return response dto
            SellerResponseDto sellerResponseDto = toSellerResponseDto(savedSeller);
            return new ResponseEntity<>(sellerResponseDto, HttpStatus.ACCEPTED);
            //endregion

        }catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //endregion

    //region Private Methods

    private List<SellerResponseDto> toSellerResponseDtos(List<Seller> sellers){
        return sellers.stream()
                .map(seller -> toSellerResponseDto(seller))
                .collect(Collectors.toList());
    }

    private SellerResponseDto toSellerResponseDto(Seller seller){
        return new SellerResponseDto(
                seller.getId(),
                seller.getCreatedAt(),
                seller.getName(),
                seller.getLastName(),
                seller.getMobileNumber());
    }

    private Seller toSellerEntity(SellerDto dto){
        return new Seller(dto.name, dto.lastName, dto.mobileNumber);
    }

    //endregion
}
