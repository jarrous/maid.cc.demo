package Maid.cc.demo.service;

import Maid.cc.demo.dto.SalesOperationDto;
import Maid.cc.demo.dto.SalesOperationResponseDto;
import Maid.cc.demo.dto.TransactionDto;
import Maid.cc.demo.dto.TransactionResponseDto;
import Maid.cc.demo.entity.*;
import Maid.cc.demo.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class SalesOperationService {

    //region Properties

    @Autowired
    SalesOperationRepository salesOperationRepository;

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    SellerRepository sellerRepository;

    private static final Logger logger = LoggerFactory.getLogger(SalesOperationService.class);

    //endregion

    //region Public Methods

    public ResponseEntity<List<SalesOperationResponseDto>> getAllSalesOperations(){
        try{

            //region get all sales operations in database
            List<SalesOperation> salesOperations = (List<SalesOperation>)salesOperationRepository.findAll();
            //endregion

            //region return list of response dtos
            List<SalesOperationResponseDto> salesOperationsResponseDtos = toSalesOperationResponseDtos(salesOperations);
            return new ResponseEntity<>(salesOperationsResponseDtos, HttpStatus.ACCEPTED);
            //endregion

        }catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<SalesOperationResponseDto> addSalesOperation(SalesOperationDto dto){
        try{

            //region convert dto to entity
            SalesOperation salesOperation = toSalesOperationEntity(dto);
            //endregion

            //region save entity and transactions

            SalesOperation savedSalesOperation = salesOperationRepository.save(salesOperation);

            List<Transaction> transactions = toTransactionsSet(dto.transactions, savedSalesOperation.getId());
            for (Transaction transaction: transactions) {
                Transaction savedTransaction = transactionRepository.save(transaction);
                savedSalesOperation.addTransaction(savedTransaction);
                savedSalesOperation = salesOperationRepository.save(savedSalesOperation);
            }

            //endregion

            //region link sales operation with client and seller

            Client client = clientRepository.findById(dto.clientId).get();
            client.addSalesOperation(savedSalesOperation);
            clientRepository.save(client);

            Seller seller = sellerRepository.findById(dto.sellerId).get();
            seller.addSalesOperation(savedSalesOperation);
            sellerRepository.save(seller);

            savedSalesOperation = salesOperationRepository.save(savedSalesOperation);
            //endregion

            //region return response dto
            SalesOperationResponseDto salesOperationResponseDto = toSalesOperationResponseDto(savedSalesOperation);
            return new ResponseEntity<>(salesOperationResponseDto, HttpStatus.ACCEPTED);
            //endregion

        }catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<SalesOperationResponseDto> updateTransaction(UUID salesOperationId, UUID transactionId, TransactionDto dto){
        try{

            //region fetch required sales operation and transaction entities
            Optional<SalesOperation> findSalesOperationResult = salesOperationRepository.findById(salesOperationId);
            if (!findSalesOperationResult.isPresent()){
                logger.error("sales operation with id = [{}] was not found", salesOperationId);
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
            SalesOperation salesOperation = findSalesOperationResult.get();

            Optional<Transaction> findTransactionResult = transactionRepository.findById(transactionId);
            if (!findTransactionResult.isPresent()){
                logger.error("transaction with id = [{}] was not found", transactionId);
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
            Transaction transaction = findTransactionResult.get();
            //endregion

            //region update transaction fields with logging

            logger.info("<< Updating transaction started >>");

            transaction.setPrice(dto.price);

            logger.info(" **** Transaction price updated");

            transaction.setQuantity(dto.quantity);

            logger.info(" **** Transaction quantity updated");

            transactionRepository.save(transaction);

            logger.info("<< Updating transaction done >>");

            //endregion

            //region return updated sales operation response dto
            SalesOperationResponseDto salesOperationResponseDto = toSalesOperationResponseDto(salesOperation);
            return new ResponseEntity<>(salesOperationResponseDto, HttpStatus.ACCEPTED);
            //endregion

        }catch (Exception ex){
            logger.error(ex.getMessage() + " , sales operation id =  [{}] , transaction id = [{}]", salesOperationId, transactionId);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //endregion

    //region Private Methods

    private List<SalesOperationResponseDto> toSalesOperationResponseDtos(List<SalesOperation> salesOperations){
        return salesOperations.stream()
                .map(salesOperation -> toSalesOperationResponseDto(salesOperation))
                .collect(Collectors.toList());
    }

    private SalesOperationResponseDto toSalesOperationResponseDto(SalesOperation salesOperation){
        return new SalesOperationResponseDto(
                salesOperation.getId(),
                salesOperation.getCreatedAt(),
                salesOperation.getClient().getId(),
                salesOperation.getClient().getName(),
                salesOperation.getSeller().getId(),
                salesOperation.getSeller().getName(),
                getTotalValue(salesOperation),
                toTransactionResponseDtos(salesOperation.getTransactions())
                );
    }

    private SalesOperation toSalesOperationEntity(SalesOperationDto dto){
        Client client = clientRepository.findById(dto.clientId).get();
        Seller seller = sellerRepository.findById(dto.sellerId).get();
        return new SalesOperation(client, seller);
    }

    private double getTotalValue(SalesOperation salesOperation){
        double total = 0.0;

        if(salesOperation.getTransactions() != null){
            for (Transaction transaction: salesOperation.getTransactions()) {
                total += transaction.getPrice() * transaction.getQuantity();
            }
        }

        return total;
    }

    private Set<TransactionResponseDto> toTransactionResponseDtos(Set<Transaction> transactions){
        HashSet<TransactionResponseDto> transactionsSet = new HashSet<>();

        for (Transaction transaction: transactions) {
            transactionsSet.add(toTransactionResponseDto(transaction));
        }
        
        return transactionsSet;
    }

    private TransactionResponseDto toTransactionResponseDto(Transaction transaction){
        return new TransactionResponseDto(
                transaction.getId(),
                transaction.getQuantity(),
                transaction.getPrice(),
                transaction.getProduct().getId(),
                transaction.getProduct().getName(),
                transaction.getSalesOperation().getId());
    }

    private List<Transaction> toTransactionsSet(List<TransactionDto> transactions, UUID salesOperationId){
        return transactions.stream()
                .map(transaction -> toTransactionEntity(transaction, salesOperationId))
                .collect(Collectors.toList());
    }

    private Transaction toTransactionEntity(TransactionDto dto, UUID salesOperationId){
        Product product = productRepository.findById(dto.productId).get();
        SalesOperation salesOperation = salesOperationRepository.findById(salesOperationId).get();
        return new Transaction(dto.quantity, dto.price, product, salesOperation);
    }

    //endregion
}
